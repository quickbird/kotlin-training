package com.quickbirdstudios.calculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val viewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calculateButton.setOnClickListener {
            val input = inputField.text.toString()

            result.text = viewModel.onMathFormulaInputChanged(input)?.toString() ?: "Das verstehe ich nicht"
        }
    }
}
