package com.quickbirdstudios.kotlinbasics


/**
 * Created by Malte Bucksch on 09/11/2017.
 */
class Dog(var name: String, val type: String = "Labrador", var age: Int = 5) {
    var owner: DogOwner? = null

    fun bark(times: Int = 3) {
        println("wufff".repeat(times))
    }

    fun sayHello(): String {
        return "Hey my name is $name"
    }
}

class DogOwner(val name: String){
    fun introduce() = print("Hi, I am $name")
}