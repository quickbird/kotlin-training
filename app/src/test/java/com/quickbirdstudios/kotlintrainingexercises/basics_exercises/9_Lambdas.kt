package com.quickbirdstudios.kotlinbasics.exercises

import com.quickbirdstudios.kotlinbasics.Dog
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Created by Malte Bucksch on 06/11/2017.
 */
class Lambdas {
    // TODO TASK 1 find and return a list of the names of all dogs
    private fun findDogNames(dogs: List<Dog>): List<String> {
        TODO("Please implement the task :-)")
    }

    // TODO TASK 2 find and return a list of all dogs whose "age" is older than 5 years
    private fun findOldDogs(dogs: List<Dog>): List<Dog> {
        TODO("Please implement the task :-)")
    }

    // TODO TASK 3 find and return a list of the NAMES of all dogs that are OLDER THAN 5
    private fun findNamesOfOldDogs(dogs: List<Dog>): List<String> {
        TODO("Please implement the task :-)")
    }

    // TODO TASK 4 find and return a list of the NAMES of all dogs that are NOT NULL and OLDER THAN 5
    private fun findNamesOfOldOptionalDogs(dogs: List<Dog?>): List<String> {
        TODO("Please implement the task :-)")
    }

    // TODO TASK 5 return the average age of all dogs
    private fun findAverageAge(dogs: List<Dog>): Double {
        TODO("Please implement the task :-)")
    }

    // TODO TASK 6 return the average age of the 3 YOUNGEST Dogs in the list
    private fun findAverageAgeOfYoungestThreeDogs(dogs: List<Dog>): Double {
        TODO("Please implement the task :-)")
    }

    /*
    Do not worry about the part below
     */
    private val dogs = listOf(Dog("Bruno", age = 4), Dog("Daisy", age = 12), Dog("Rex", age = 7))

    @Test
    fun testDogNamesUsingMap() {
        val names = findDogNames(dogs)

        assertEquals(listOf("Bruno", "Daisy", "Rex"), names)
    }

    @Test
    fun testOldDogsUsingFilter() {
        val oldDogs = findOldDogs(dogs)

        for (dog in oldDogs) {
            assertTrue(dog.age > 5)
        }
    }

    @Test
    fun testFindNamesOfOldDogs() {
        val oldDogNames = findNamesOfOldDogs(dogs)

        assertEquals(listOf("Daisy", "Rex"), oldDogNames)
    }

    @Test
    fun testFindNamesOfOldOptionalDogs() {
        val oldDogNames = findNamesOfOldOptionalDogs(dogs + listOf<Dog?>(null, null, Dog("Hasso", age = 6)))

        assertEquals(listOf("Daisy", "Rex", "Hasso"), oldDogNames)
    }

    @Test
    fun testFindAverageAge() {
        val average = findAverageAge(dogs)

        assertEquals((4.0 + 12.0 + 7.0)/3.0, average, 0.001)
    }

    @Test
    fun testFindAverageAgeOfYoungestThreeDogs() {
        val average = findAverageAgeOfYoungestThreeDogs(dogs + listOf(Dog("Daisy", age = 12), Dog("Rex", age = 1)))

        assertEquals(4.0, average, 0.001)
    }
}