package com.quickbirdstudios.kotlinintro.exercises

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Malte Bucksch on 05/11/2017.
 */

class KotlinDog(name: String, var type: String = "Labrador", var age: Int = 5) {
    //    TODO TASK 1 create getter for "name" which returns the name as upperCase/all-caps (HINT: myText.toUpperCase())
    //    TODO TASK 2 create setter for name: Throw an "IllegalArgumentException" if the new name is empty
    //    HINT: throwing exceptions happens like in java with `throw IllegalArgumentException(...)`
    var name = name

    //    TODO TASK 3 throw an IllegalArgumentException if somebody tries to assign a negative value to "age"

    //    TODO TASK 4 add the prefix "Type: " before every "type" by adapting the "type" getter

}


/*
  Do not worry about the part below!
   */
class DogGetterSetter {
    private val dog = KotlinDog("bruno", "Labrador");

    @Test
    fun testDogNameIsCapitalized() {
        assertEquals("BRUNO", dog.name)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testEmptyDogNameNotAllowed() {
        dog.name = ""
    }

    @Test(expected = IllegalArgumentException::class)
    fun testNegativeAgeNotAllowed() {
        dog.age = -1
    }

    @Test
    fun testDogTypePrefix() {
        dog.type = "Labrador"
        assertEquals("Type: Labrador", dog.type)

        dog.type = "Dackel"
        assertEquals("Type: Dackel", dog.type)
    }
}

