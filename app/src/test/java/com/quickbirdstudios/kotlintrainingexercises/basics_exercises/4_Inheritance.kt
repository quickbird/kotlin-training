package com.quickbirdstudios.kotlintrainingexercises.basics_exercises

import org.junit.Test


/**
 * Created by Malte Bucksch on 11/12/2017.
 */
//TODO TASK 1 create an -abstract- Animal class with a property "size" (Int)
//TODO TASK add an abstract method "makeSound" (without any return value)
//TODO TASK add a companion object with a property "MAX_SIZE" (=10)
//TODO TASK add a method "grow" which increases the "size" of the animal IF the size is smaller than "MAX_SIZE"


// add code here for TASK 1...

//TODO TASK 2 create an interface "Flyable" with a method "fly" and a property "flyingSpeed"


// add code here for TASK 2...


//TODO TASK 3 create a sub class of Animal called "Bird" (calling the super constructor for the bird "size")
//TODO TASK override the method "makeSound" and print the sound of a bird (which is "fiep" of course)
//TODO TASK implement the interface "Flyable"
//TODO TASK -> override the property "flyingSpeed" with a value of 10
//TODO TASK -> override the method "fly" which prints "flying at speed *flyingSpeed*... /^v^\\"

// add code here for TASK 3...


class InheritanceTests {
    @Test
    fun testBird() {
        //TODO TASK 8 create a Bird instance with "size" of 2
        //TODO TASK 9 let the bird "grow", let him "fly", and let him "makeSound"
        TODO("Please implement the task :-)")
    }
}
