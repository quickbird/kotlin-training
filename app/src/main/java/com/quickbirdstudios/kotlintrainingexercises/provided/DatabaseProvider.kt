package com.quickbirdstudios.kotlintrainingexercises.provided


/**
 * Created by Malte Bucksch on 13/12/2017.
 */
object DatabaseProvider {
    fun fetchGermanSeaAnimals(): List<String> =
            listOf("Qualle", "See-Stern", "Tunfisch", "Flunder", "Karpfen", "Hai", "Delfin")

    fun fetchEnglishSeaAnimals() =
            listOf("Jelly fish", "Sea star", "Tuna", "Flounder", "Carp", "Shark", "Dolphin")
}
